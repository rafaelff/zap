<!--
SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Licensing information

- The source code is released under the terms of the [GNU General Public License 3.0 or later](./LICENSES/GPL-3.0-or-later.txt).
- The original artwork and translations are released under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International](./LICENSES/CC-BY-SA-4.0.txt).
- Additional icons from the [Icon Development Kit](https://gitlab.gnome.org/Teams/Design/icon-development-kit) of the [GNOME Design Team](https://gitlab.gnome.org/Teams/Design) are released under the terms of the [CC0 1.0 Universal](./LICENSES/CC0-1.0.txt).
- The [Applause sound](https://opengameart.org/content/applause-in-a-large-hall-or-church) from [eXpl0it3r](https://opengameart.org/users/expl0it3r) is released under the terms of the [CC0 1.0 Universal](./LICENSES/CC0-1.0.txt).
- The Bark sound from the [GNOME Settings Team](https://gitlab.gnome.org/GNOME/gnome-control-center/) is released under the terms of the [GNU General Public License 2.0](./LICENSES/GPL-2.0-only.txt).
- The [Theme Song 8-bit](https://opengameart.org/content/theme-song-8-bit) from [nene](https://opengameart.org/users/nene) is released under the terms of the [CC0 1.0 Universal](./LICENSES/CC0-1.0.txt).

This project is [REUSE](https://reuse.software/)-compliant, each file's licensing information is annotated with [SPDX](https://spdx.dev/).
